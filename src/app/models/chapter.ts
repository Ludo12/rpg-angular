import {Armor, Hero, Monster, Weapon} from './characters';

export enum CharcterAction {
  attack = 'Attaque',
  sneak = 'Discretion',
  persuade = 'Persuation',
  doNothing = 'Ne rien faire'
}

export enum FaillureOptions {
  gameOver,
  nextChapter
}

export enum SuccesOptions {
  rewardExperience,
  rewardEquipement,
  addHeroToParty
}

export class Chapter {
  story: string[];
  options: CharcterAction[];
  enemy: Monster[];
  sneakPersuadeFail: CharcterAction;
  ifFail: FaillureOptions;
  ifSucced: {
    experience: number,
    equipement: (Weapon | Armor)[],
    newHero: Hero
  };
  nextChapter: Chapter;
}
