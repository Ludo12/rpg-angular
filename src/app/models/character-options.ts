export enum RaceOptions {
  human = 'Humain',
  dwarf = 'Nain',
  elf = 'Elf',
  halfling = 'Semi-Homme'
}

export enum ClassOptions {
  warrior = 'Guerrier',
  hunter = 'Chasseur',
  rogue = 'Voleur',
  priest = 'Prêtre'
}

export enum GenderOptions {
  male = 'Homme',
  female = 'Femme'
}

export const CharacterOptions = {
  races: [
    RaceOptions.human,
    RaceOptions.dwarf,
    RaceOptions.elf,
    RaceOptions.halfling
  ],
  classes: [
    ClassOptions.warrior,
    ClassOptions.hunter,
    ClassOptions.rogue,
    ClassOptions.priest
  ],
  genders: [
    GenderOptions.male,
    GenderOptions.female
  ]
};
