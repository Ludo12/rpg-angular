import {RaceOptions, ClassOptions, GenderOptions} from './character-options';

export class Armor {
  constructor(name: string, attackBarrierBonus: number) {
    this.name = name;
    this.attackBarrierBonus = attackBarrierBonus;
  }

  name: string;
  attackBarrierBonus: number;
}

export class Weapon {
  constructor(name: string, minDamage: number, maxDamage: number) {
    this.name = name;
    this.minDamage = minDamage;
    this.maxDamage = maxDamage;
  }

  name: string;
  minDamage: number;
  maxDamage: number;
}

export enum CharacterSkils {
  attack = 'attaque',
  sneak = 'discretion',
  persuade = 'persuation',
  intelligence = 'intelligence'
}

export enum FightSkills {
  attack = 'Attaque',
  specialAttack = 'Attaque Speciale',
  none = 'rien'
}

export const ExpereinceToLevel = {
  1: 1000,
  2: 2000,
  3: 3000,
  4: 4000,
  5: 5000,
  6: 6000,
  7: 7000,
  8: 8000,
  9: 9000
};

export class BaseCharacter {
  name: string;
  maxHealth: number;
  currentHealth: number;
  isIncapacited: boolean;
  barriers: {
    attack: number;
    sneak: number;
    persuade: number;
  };
  skills: {
    attack: number;
    sneak: number;
    persuade: number;
    intelligence: number;
  };
  equippedWeapon: Weapon;
  equippedArmor: Armor;

  constructor(name: string, health: number, skills = {attack: 0, sneak: 0, persuade: 0, intelligence: 0}) {
    this.name = name;
    this.maxHealth = health;
    this.currentHealth = health;
    this.skills = skills;
    this.isIncapacited = false;
    this.barriers = {
      attack: 10,
      sneak: 10,
      persuade: 10
    };
  }

  attack() {
    return Math.floor(Math.random() * 20) + 1 + this.skills.attack;
  }

  sneak() {
    return Math.floor(Math.random() * 20) + 1 + this.skills.sneak;
  }

  persuade() {
    return Math.floor(Math.random() * 20) + 1 + this.skills.persuade;
  }

  dealDamage() {
    return Math.floor(Math.random() * (this.equippedWeapon.maxDamage - this.equippedWeapon.minDamage + 1) + this.equippedWeapon.minDamage);
  }
}

export class Hero extends BaseCharacter {
  gender: string;
  race: string;
  characterRole: string;
  experience: number;
  level: number;
  availableSkillsPoints: number;
  hasTrapDefence: boolean;
  hasDamagingTrap: boolean;
  turnUntilSpecialAvailableAgain: number;

  constructor(name: string, gender: string, race: string, level: number, health: number, skills, weapon, armor) {
    super(name, health, skills);
    this.gender = gender;
    this.race = race;
    this.experience = 0;
    this.level = level;
    this.equippedNewWeapon = weapon;
    this.equippedNewArmor(armor);
  }

  levelUp() {
    this.experience -= ExpereinceToLevel[this.level];
    this.level++;
    this.availableSkillsPoints += 2;
    if (this.experience >= ExpereinceToLevel[this.level]) {
      this.levelUp();
    }
  }

  equippedNewArmor(armor: Armor) {
    if (this.equippedArmor) {
      this.barriers.attack -= this.equippedArmor.attackBarrierBonus;
    }
    this.equippedArmor = armor;
    this.barriers.attack += this.equippedArmor.attackBarrierBonus;
  }

  equippedNewWeapon(weapon: Weapon) {
    this.equippedWeapon = weapon;
  }

  rest() {
    this.currentHealth = this.maxHealth;
    this.isIncapacited = false;
    this.turnUntilSpecialAvailableAgain = 0;
  }
}

export class Monster extends BaseCharacter {
  isTrapped = false;
  poisoStacks = 0;
  isStrongPoison = false;
  hasTakenPoisonDamageThisTurn = false;

  constructor(name, health, skills, barriers: {
                attack: number,
                sneak: number,
                persuade: number
              },
              minDamage: number, maxDamage: number) {
    super(name, health, skills);
    this.barriers = barriers;
    this.equippedWeapon = new Weapon(undefined, minDamage, maxDamage);
  }
}

export class Warrior extends Hero {
  constructor(name, gender, race, level, health, skills, weapon, armor) {
    super(name, gender, race, level, health, skills, weapon, armor);
    this.characterRole = ClassOptions.warrior;
    this.skills.attack += 2;
    this.skills.sneak--;
    this.skills.persuade++;
    this.skills.intelligence--;
  }

  levelUp() {
    this.maxHealth = Math.floor(Math.random() * 10) + 1;
    this.currentHealth = this.maxHealth;
    super.levelUp();
  }
}

export class Hunter extends Hero {
  constructor(name, gender, race, level, health, skills, weapon, armor) {
    super(name, gender, race, level, health, skills, weapon, armor);
    this.characterRole = ClassOptions.hunter;
    this.skills.sneak += 2;
    this.skills.intelligence++;
    this.skills.persuade--;
    this.skills.attack--;
  }

  levelUp() {
    this.maxHealth = Math.floor(Math.random() * 8) + 1;
    this.currentHealth = this.maxHealth;
    super.levelUp();
  }
}

export class Rogue extends Hero {
  constructor(name, gender, race, level, health, skills, weapon, armor) {
    super(name, gender, race, level, health, skills, weapon, armor);
    this.characterRole = ClassOptions.hunter;
    this.skills.attack += 2;
    this.skills.sneak++;
    this.skills.persuade--;
    this.skills.intelligence--;
  }

  levelUp() {
    this.maxHealth = Math.floor(Math.random() * 8) + 1;
    this.currentHealth = this.maxHealth;
    super.levelUp();
  }
}

export class Priest extends Hero {
  constructor(name, gender, race, level, health, skills, weapon, armor) {
    super(name, gender, race, level, health, skills, weapon, armor);
    this.characterRole = ClassOptions.hunter;
    this.skills.intelligence += 2;
    this.skills.persuade++;
    this.skills.sneak--;
    this.skills.attack--;
  }

  levelUp() {
    this.maxHealth = Math.floor(Math.random() * 6) + 1;
    this.currentHealth = this.maxHealth;
    super.levelUp();
  }
}

export const checkRace = (hero: Hero) => {
  switch (hero.race) {
    case RaceOptions.human:
      hero.skills.persuade += 2;
      hero.skills.intelligence++;
      hero.skills.sneak -= 2;
      break;
    case RaceOptions.elf:
      hero.skills.intelligence += 2;
      hero.skills.sneak++;
      hero.skills.persuade -= 2;
      break;
    case RaceOptions.dwarf:
      hero.skills.attack += 2;
      hero.skills.persuade++;
      hero.skills.intelligence -= 2;
      break;
    case RaceOptions.halfling:
      hero.skills.sneak += 2;
      hero.skills.attack++;
      hero.skills.persuade -= 2;
      break;
    default:
      break;
  }
};
