import {Component, OnInit} from '@angular/core';
import {CharacterOptions} from '../../models/character-options';

@Component({
  selector: 'app-character-creation',
  templateUrl: './character-creation.component.html',
  styleUrls: ['./character-creation.component.css']
})
export class CharacterCreationComponent implements OnInit {
  character = {
    race: '--Choisir--',
    class: '--Choisir--',
    gender: undefined,
    name: undefined
  };
  characterComplete: boolean = false;
  races = CharacterOptions.races;
  classes = CharacterOptions.classes;
  genders = CharacterOptions.genders;

  constructor() {
  }

  ngOnInit(): void {
  }

  changeRace(race: string) {
    this.character.race = race;
    this.checkCompleted();
  }

  changeClass(newClass: string) {
    this.character.class = newClass;
    this.checkCompleted();
  }

  changeGender(gender: string) {
    this.character.gender = gender;
    this.checkCompleted();
  }

  changeName() {
    this.checkCompleted();
  }

  checkCompleted() {
    this.characterComplete = this.character.race !== '--Choisir--'
      && this.character.class !== '--Choisir--'
      && this.character.gender !== undefined
      && this.character.name;
  }

  createCharacter() {
    if (!this.checkCompleted) {
      return;
    }
    console.log(this.character);
  }
}
