import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Armor, Hero, Monster, Warrior, Weapon} from './models/characters';
import {Chapter} from './models/chapter';
import {ChapterOne} from './character/chapterOne';
import {ClassOptions} from './models/character-options';

@Injectable({
  providedIn: 'root'
})
export class GameControllerService {

  constructor(private router: Router) {
  }

  mainCharacter: Hero;
  currentChapter: Chapter = ChapterOne;
  isFighting = false;

  actionDelay: 1500;
  heroParty: Hero[] = [];
  partyInventory: (Weapon | Armor) [] = [];
  availableHeroes: Hero[] = [];
  enemy: Monster[] = this.currentChapter.enemy;

  setMainCharacter(character) {
    switch (character.class) {
      case ClassOptions.warrior:
        this.mainCharacter = new Warrior(character.name, character.gender, character.race, 1, 10, {
            attack: 0, sneak: 0, persuade: 0, intellignece: 0
          },
          new Weapon('Couteau', 1, 3),
          new Armor('habits', 0));
        break;
      default:
        break;


    }
  }
}
