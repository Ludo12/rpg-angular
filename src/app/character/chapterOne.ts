import {Chapter, CharcterAction, FaillureOptions, SuccesOptions} from '../models/chapter';
import {Armor, Monster, Warrior, Weapon} from '../models/characters';
import {GenderOptions, RaceOptions} from '../models/character-options';

// @ts-ignore
export const ChapterOne: Chapter = {
  // tslint:disable-next-line:max-line-length
  story: ['Dans un trou vivait un Hobbit ..'],
  options: [
    CharcterAction.attack,
    CharcterAction.sneak,
    CharcterAction.persuade
  ],
  enemy: [
    new Monster('Goblin', 5, {
        attack: 2, sneak: 0, persuade: 0
      }, {attack: 3, sneak: 2, persuade: 0},
      1,
      3
    )
  ],
  sneakPersuadeFail: CharcterAction.attack,
  ifFail: FaillureOptions.nextChapter,
  ifSucced: [
    SuccesOptions.rewardExperience,
    SuccesOptions.rewardEquipement,
    SuccesOptions.addHeroToParty
  ],
  rewards: {
    experience: 500,
    equipement: [
      new Weapon('Lame rouillé', 1, 6)
    ],
    newHero: new Warrior('Klaus',
      GenderOptions.male,
      RaceOptions.human,
      1,
      10,
      {attack: 2, sneak: 1, persuade: 1, intelligence: 1},
      new Weapon('épée courte', 1, 4),
      new Armor('Tunique de chasseur', 1))
  },
  nextChapter: null
};
